from app.services.gather_labeled_dataset.utils.compare_labeled_original_dataset import FolderComparison
from app.services.gather_labeled_dataset.utils.update_fp_labour_action import FpLabourAction


class GatherLabeledDataset:
    def __init__(self, yaml_data):
        self.labeled_folder_path = yaml_data['labeled_folder_path']
        self.original_folder_path = yaml_data['original_folder_path']
        self.mlops_db_params = yaml_data['mlops_db_params']

    def execute_gather_labeled_dataset(self):
        print('Start sort labeled and deleted images')
        folder_comparison = FolderComparison(self.labeled_folder_path, self.original_folder_path)
        whole_deleted_source_id_list, whole_labelled_source_id_list = (
            folder_comparison.find_all_labeled_deleted_images())

        print('Start update fp_db LABOUR_ACTION')
        fp_labour_action = FpLabourAction(whole_deleted_source_id_list, whole_labelled_source_id_list,
                                          self.mlops_db_params)
        fp_labour_action.update_fp_db()
