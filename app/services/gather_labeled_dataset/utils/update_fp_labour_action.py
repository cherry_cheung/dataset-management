from datetime import datetime, timezone
from sqlalchemy import update, select

from app.models.mlops import FPStaging
from app.utils.db_connection import high_level_postgres_connect, low_level_postgres_connect
import uuid
from app.utils.log_error import *


class FpLabourAction:
    def __init__(self, whole_deleted_source_id_list, whole_labeled_source_id_list, db_params):
        self.whole_deleted_source_id_list = whole_deleted_source_id_list
        self.whole_labeled_source_id_list = [uuid.UUID(source_id) for source_id in whole_labeled_source_id_list]
        self.whole_deleted_source_id_list = [uuid.UUID(source_id) for source_id in whole_deleted_source_id_list]
        self.db_params = db_params

    def checker(self):
        whole_source_id_list = self.whole_deleted_source_id_list + self.whole_labeled_source_id_list
        with low_level_postgres_connect(self.db_params) as session:
            for source_id in whole_source_id_list:
                stmt = select(FPStaging).where(
                    (FPStaging.source_id == source_id) & (FPStaging.labour_action_type.isnot(None))
                )
                result = session.execute(stmt)
                matching_record = result.fetchone()

                if matching_record:
                    raise ValueError(f"For source_id {source_id}, labour_action_type is not NULL.")

    def update_fp_db(self):
        self.checker()
        for source_id in self.whole_deleted_source_id_list:
            try:
                with high_level_postgres_connect(self.db_params) as session:
                    update_stmt = update(FPStaging).where(FPStaging.source_id == source_id).values(
                        labour_action_date=datetime.now(timezone.utc),
                        labour_action_type='deleted',
                        updated_at=datetime.now(timezone.utc)
                    )
                    session.execute(update_stmt)
                    session.commit()
            except:
                logging.warning(f'Fail to update_fp_db: {source_id}')

        for source_id in self.whole_labeled_source_id_list:
            try:
                with high_level_postgres_connect(self.db_params) as session:
                    update_stmt = update(FPStaging).where(FPStaging.source_id == source_id).values(
                        labour_action_date=datetime.now(timezone.utc),
                        labour_action_type='labeled',
                        updated_at=datetime.now(timezone.utc)
                    )
                    session.execute(update_stmt)
                    session.commit()
            except:
                logging.warning(f'Fail to update_fp_db: {source_id}')
