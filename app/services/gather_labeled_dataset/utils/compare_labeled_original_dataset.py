import os


class FolderComparison:
    def __init__(self, labeled_folder_dir, original_folder_dir):
        self.labeled_folder_dir = labeled_folder_dir
        self.original_folder_dir = original_folder_dir

    def find_deleted_images(self, labeled_images_folder, original_images_folder):
        labeled_images_set = set(os.listdir(labeled_images_folder))
        original_images_set = set(os.listdir(original_images_folder))

        deleted_images_list = list(original_images_set - labeled_images_set)
        deleted_images_source_id_list = [item[:-4].split('_')[-1] for item in deleted_images_list]
        labeled_images_list = list(labeled_images_set)
        labeled_images_source_id_list = [item[:-4].split('_')[-1] for item in labeled_images_list]

        if (len(deleted_images_source_id_list) + len(labeled_images_source_id_list)) != len(list(original_images_set)):
            raise Exception('Incorrect Number of Images')

        return deleted_images_source_id_list, labeled_images_source_id_list

    def find_all_labeled_deleted_images(self):
        original_folder_path_list = []
        labeled_folder_path_list = []

        whole_deleted_source_id_list = []
        whole_labeled_source_id_list = []

        for sub_folder in os.listdir(self.original_folder_dir):
            original_images_folder_path = os.path.join(self.original_folder_dir, sub_folder)
            original_folder_path_list.append(original_images_folder_path)

        for sub_folder in os.listdir(self.labeled_folder_dir):
            labeled_images_folder_path = os.path.join(self.labeled_folder_dir, sub_folder, 'images')
            labeled_folder_path_list.append(labeled_images_folder_path)

        if len(original_folder_path_list) != len(labeled_folder_path_list):
            raise Exception('Number of original project != Number of labeled project')

        for i in range(len(original_folder_path_list)):
            deleted_source_id_list, labelled_source_id_list = (
                self.find_deleted_images(labeled_folder_path_list[i], original_folder_path_list[i]))

            whole_labeled_source_id_list += labelled_source_id_list
            whole_deleted_source_id_list += deleted_source_id_list

        return whole_deleted_source_id_list, whole_labeled_source_id_list
