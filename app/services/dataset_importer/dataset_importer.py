from app.services.dataset_importer.utils.upload_s3 import S3Uploader
from app.services.dataset_importer.utils.upload_s3_checker import S3UploadChecker
from app.services.dataset_importer.utils.update_fp_staging_table import FpUploadTime
from app.services.dataset_importer.utils.update_dataset_table import InsertDataset


class DatasetImporter:
    def __init__(self, yaml_data):
        self.labeled_folder_path = yaml_data['labeled_folder_path']
        self.aws_credential = yaml_data['aws_credential']
        self.mlops_db_params = yaml_data['mlops_db_params']
        self.mlops_schema_name = yaml_data['mlops_schema_name']

    def execute_import_dataset(self):
        print('start importing dataset to s3')
        s3_uploader = S3Uploader(self.aws_credential, self.labeled_folder_path)
        s3_uploader.s3_upload_all_projects()

        print('start check all files uploaded to s3 successfully')
        s3_upload_checker = S3UploadChecker(self.aws_credential, self.labeled_folder_path)
        uploaded_source_id_list, s3_images_url_list = s3_upload_checker.check_all_s3_file_exist()

        print('start update fp staging table - uploaded_at')
        fp_upload_time = FpUploadTime(uploaded_source_id_list, self.mlops_db_params)
        fp_upload_time.update_fp_db()

        print('start insert dataset table')
        insert_dataset = InsertDataset(self.mlops_db_params, self.mlops_schema_name,
                                       uploaded_source_id_list, s3_images_url_list)
        insert_dataset.insert_dataset_table()
