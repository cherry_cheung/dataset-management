import os
import boto3
from botocore.exceptions import ClientError
from app.utils.log_error import *


class S3UploadChecker:
    def __init__(self, aws_param, labeled_folder_path):
        self.aws_access_key_id = aws_param['aws_access_key_id']
        self.aws_secret_access_key = aws_param['aws_secret_access_key']
        self.region_name = aws_param['region_name']
        self.bucket_name = aws_param['bucket_name']
        self.labeled_folder_path = labeled_folder_path

    def check_one_s3_file_exists(self, key):
        s3 = boto3.client('s3', aws_access_key_id=self.aws_access_key_id,
                          aws_secret_access_key=self.aws_secret_access_key,
                          region_name=self.region_name)
        try:
            s3.head_object(Bucket=self.bucket_name, Key=key)
            return True
        except ClientError as e:
            if e.response['Error']['Code'] == '404':
                return False
            else:
                raise

    def check_all_s3_file_exist(self):
        uploaded_source_id_list = []
        image_s3_path_list = []

        for project_name in os.listdir(self.labeled_folder_path):
            project_image_folder_path = os.path.join(self.labeled_folder_path, project_name, 'images')
            for image_name in os.listdir(project_image_folder_path):
                try:
                    label_name = image_name[:-4] + '.txt'
                    image_s3_path = project_name + '/images/' + image_name
                    label_s3_path = project_name + '/labels/' + label_name
                    image_boolean = self.check_one_s3_file_exists(image_s3_path)
                    label_boolean = self.check_one_s3_file_exists(label_s3_path)
                    if image_boolean and label_boolean:
                        uploaded_source_id_list.append(image_name[:-4].split('_')[-1])
                        image_s3_path_list.append(self.bucket_name + '/' + image_s3_path)
                except:
                    logging.warning(f'Fail to check s3 file: Variable: {image_name}')

        return uploaded_source_id_list, image_s3_path_list
