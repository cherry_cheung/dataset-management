import uuid
import pandas as pd
from sqlalchemy import select
from datetime import datetime, timezone

from app.utils.db_connection import high_level_postgres_connect, low_level_postgres_connect
from app.models.mlops import FPStaging
from app.utils.log_error import *


class InsertDataset:
    def __init__(self, db_params, schema_name, uploaded_source_id_list, s3_image_ref_list):
        self.db_params = db_params
        self.uploaded_source_id_list = uploaded_source_id_list
        self.s3_image_ref_list = s3_image_ref_list
        self.schema_name = schema_name

    def query_fp_staging_table(self):
        with high_level_postgres_connect(self.db_params) as session:
            query = (
                select(FPStaging.id, FPStaging.model_id).
                filter(
                    FPStaging.source_id.in_(self.uploaded_source_id_list)
                )
            )
            filtered_results = session.execute(query).fetchall()

            return filtered_results

    def prepare_new_dataset(self, filter_results):
        id_list = []
        fp_staging_id_list = []
        model_id_list = []
        split_list = []
        source_type_list = []
        ref_url_list = []
        created_at_list = []
        updated_at_list = []

        counter = 0
        for row in filter_results:
            try:
                id_list.append(str(uuid.uuid4()))
                fp_staging_id = row[0]
                fp_staging_id_list.append(fp_staging_id)
                model_id = row[1]
                model_id_list.append(model_id)
                split_list.append('train')
                source_type_list.append('fp')
                ref_url_list.append(self.s3_image_ref_list[counter])
                created_at_list.append(datetime.now(timezone.utc))
                updated_at_list.append(datetime.now(timezone.utc))
                counter += 1
            except:
                logging.warning(f'Fail to insert sample to dataset: Variable: {row}')

        df = pd.DataFrame({
            'id': id_list,
            'fp_staging_id': fp_staging_id_list,
            'model_id': model_id_list,
            'split': split_list,
            'source_type': source_type_list,
            'ref_url': ref_url_list,
            'created_at': created_at_list,
            'updated_at': updated_at_list
        })

        return df

    def insert_dataset_table(self):
        filter_results = self.query_fp_staging_table()
        df = self.prepare_new_dataset(filter_results)
        with low_level_postgres_connect(self.db_params) as session:
            df.to_sql(name='dataset', con=session, schema=self.schema_name, if_exists='append', index=False)
