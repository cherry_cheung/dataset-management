from datetime import datetime, timezone
from sqlalchemy import update

from app.models.mlops import FPStaging
from app.utils.db_connection import high_level_postgres_connect
from app.utils.log_error import *


class FpUploadTime:
    def __init__(self, uploaded_source_id_list, db_params):
        self.uploaded_source_id_list = uploaded_source_id_list
        self.db_params = db_params

    def update_fp_db(self):
        for source_id in self.uploaded_source_id_list:
            try:
                with high_level_postgres_connect(self.db_params) as session:
                    update_stmt = update(FPStaging).where(FPStaging.source_id == source_id).values(
                        uploaded_at=datetime.now(timezone.utc),
                        updated_at=datetime.now(timezone.utc)
                    )
                    session.execute(update_stmt)
                    session.commit()
            except:
                logging.warning(f'Fail to update_fp_db: Variable: {source_id}')
