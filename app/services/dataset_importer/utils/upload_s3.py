import os
import boto3
from app.utils.log_error import *


class S3Uploader:
    def __init__(self, aws_param, labeled_folder_path):
        self.aws_access_key_id = aws_param['aws_access_key_id']
        self.aws_secret_access_key = aws_param['aws_secret_access_key']
        self.region_name = aws_param['region_name']
        self.bucket_name = aws_param['bucket_name']
        self.labeled_folder_path = labeled_folder_path

    def s3_upload_one_folder(self, s3_folder, local_folder_path):
        s3 = boto3.client('s3', aws_access_key_id=self.aws_access_key_id,
                          aws_secret_access_key=self.aws_secret_access_key,
                          region_name=self.region_name)
        for root, dirs, files in os.walk(local_folder_path):
            for file in files:
                try:
                    local_file_path = os.path.join(root, file)
                    s3_key = os.path.join(s3_folder, os.path.relpath(local_file_path, local_folder_path))
                    s3.upload_file(local_file_path, self.bucket_name, s3_key)
                except:
                    logging.warning(f'Fail to upload file to s3: Variable: {file}')

    def s3_upload_all_projects(self):
        for project_id in os.listdir(self.labeled_folder_path):
            try:
                images_folder_dir = os.path.join(self.labeled_folder_path, project_id, 'images')
                images_s3_folder = project_id + '/images'
                self.s3_upload_one_folder(images_s3_folder, images_folder_dir)
                labels_folder_dir = os.path.join(self.labeled_folder_path, project_id, 'labels')
                labels_s3_folder = project_id + '/labels'
                self.s3_upload_one_folder(labels_s3_folder, labels_folder_dir)
            except:
                logging.warning(f'Fail to upload project to s3: Variable: {project_id}')
