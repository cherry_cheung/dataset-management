import uuid

from sqlalchemy import Column, DateTime, Text, Enum, ForeignKey
from enum import Enum as EnumPy

from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID

from app.utils.db_connection import Base

# TODO: checking of relationship


class JobStatus(EnumPy):
    labeled = 'labeled'
    deleted = 'deleted'


class Splits(EnumPy):
    train = 'train'
    valid = 'valid'
    test = 'test'


class SourceType(EnumPy):
    base = 'base'
    legacy = 'legacy'
    fp = 'fp'
    fn = 'fn'


class FPStaging(Base):
    __tablename__ = "fp_staging"
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4, unique=True, nullable=False)
    photo_url = Column(Text)
    labour_action_date = Column(DateTime(timezone=True))
    updated_at = Column(DateTime(timezone=True))
    dispatched_at = Column(DateTime(timezone=True))
    source_id = Column(UUID(as_uuid=True), default=uuid.uuid4)
    labour_action_type = Column(Enum(JobStatus))
    uploaded_at = Column(DateTime(timezone=True))
    model_id = Column(Text)

    datasets = relationship('Dataset', back_populates='fp_staging')


class Dataset(Base):
    __tablename__ = "dataset"
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4, unique=True, nullable=False)
    fp_staging_id = Column(UUID(as_uuid=True), ForeignKey('fp_staging.id'))
    model_id = Column(Text)
    split = Column(Enum(Splits))
    source_type = Column(Enum(SourceType))
    on_boarding_date = Column(DateTime(timezone=True))
    created_at = Column(DateTime(timezone=True))
    updated_at = Column(DateTime(timezone=True))

    fp_staging = relationship('FPStaging', back_populates='datasets')
