from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, declarative_base
from contextlib import contextmanager


class Base:
    __allow_unmapped__ = True


Base = declarative_base(cls=Base)


@contextmanager
def low_level_postgres_connect(db_params):
    connection_string = (
        f"postgresql+psycopg2://{db_params['username']}:{db_params['password']}"
        f"@{db_params['host']}:{db_params['port']}/"
        f"{db_params['database']}")

    engine = create_engine(connection_string)
    connection = engine.connect()
    try:
        yield connection
    finally:
        connection.close()


@contextmanager
def high_level_postgres_connect(db_params):
    connection_string = (f"postgresql+psycopg2://{db_params['username']}:{db_params['password']}@"
                         f"{db_params['host']}:{db_params['port']}/{db_params['database']}")

    engine = create_engine(connection_string)
    Session = sessionmaker(bind=engine)

    connection = engine.connect()
    try:
        yield Session()
    finally:
        connection.close()
