import os
import shutil


def split_folder(source_folder_path):
    '''
    [YoloLabel Helper]
    The images and labels currently reside in a shared directory.
    This function will be used to separate them into two distinct folders: one for images and another for labels.
    '''
    label_folder = os.path.join(source_folder_path, 'labels')
    image_folder = os.path.join(source_folder_path, 'images')
    os.makedirs(label_folder, exist_ok=True)
    os.makedirs(image_folder, exist_ok=True)

    for file in os.listdir(source_folder_path):
        file_path = os.path.join(source_folder_path, file)
        if file.endswith('.txt'):
            new_label_path = os.path.join(label_folder, file)
            shutil.move(file_path, new_label_path)
        elif file.endswith(('.jpg', '.png', '.jpeg')):
            shutil.move(file_path, os.path.join(image_folder, file))

    if len(os.listdir(image_folder)) == len(os.listdir(label_folder)):
        return True
    else:
        return False

# # example
# dataset_folder = r'/home/cherry/Downloads/images03h1.5'
# checker = split_folder(dataset_folder)
# print('The number of images = The number of labels', checker)
