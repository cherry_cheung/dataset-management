import yaml


def read_config(yaml_folder_path):
    with open(yaml_folder_path, 'r') as file:
        yaml_data = yaml.safe_load(file)
        config = {
            'aws_credential': {
                'aws_access_key_id': yaml_data['aws_credential']['aws_access_key_id'],
                'aws_secret_access_key': yaml_data['aws_credential']['aws_secret_access_key'],
                'region_name': yaml_data['aws_credential']['region_name'],
                'bucket_name': yaml_data['aws_credential']['bucket_name']
            },
            'mlops_db_params': {
                'database': yaml_data['mlops_db_params']['database'],
                'port': yaml_data['mlops_db_params']['port'],
                'username': yaml_data['mlops_db_params']['username'],
                'host': yaml_data['mlops_db_params']['host'],
                'password': yaml_data['mlops_db_params']['password']
            },
            'mlops_schema_name': yaml_data['mlops_schema_name'],
            'labeled_folder_path': yaml_data['labeled_folder_path'],
            'original_folder_path': yaml_data['original_folder_path']
        }

        return config
