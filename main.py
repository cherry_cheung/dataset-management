import os

from app.services.gather_labeled_dataset.gather_labeled_dataset import GatherLabeledDataset
from app.services.dataset_importer.dataset_importer import DatasetImporter
from app.utils.utils import read_config


def main():
    yaml_folder_path = os.path.join(os.getcwd(), 'config.yaml')
    yaml_data = read_config(yaml_folder_path)

    gather_labeled_dataset = GatherLabeledDataset(yaml_data)
    gather_labeled_dataset.execute_gather_labeled_dataset()

    dataset_importer = DatasetImporter(yaml_data)
    dataset_importer.execute_import_dataset()


if __name__ == "__main__":
    main()
