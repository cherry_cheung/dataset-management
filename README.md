# Dataset Management
![dataset_management.drawio.svg](docs/dataset_management.drawio.svg)

## How to run the repo?

Put the labeled and original dataset in the following folder structure:
```
- labeled_folder_path
  - project_id_1
    - images
       - images_1.jpg
       - ...
    - labels
       - images_1.txt
       - ...
  - ...
- original_folder_path
  - project_id_1
    - images_1.jpg
    - ...
  - ...
```

### config template

```
mlops_db_params: {
   'database': "",
   'port': 5432,
   'username': "",
   'host': "",
   'password': ""
}

mlops_schema_name: 'public'

aws_credential: {
                  aws_access_key_id: "",
                  aws_secret_access_key: "",
                  region_name: 'ap-southeast-1',
                  bucket_name: 'varadise-mlops-image-uploader-test'
}
labeled_folder_path: ''
original_folder_path: ''
```

```
touch config.yaml
# Adhere to the instructions and insert the relevant information into the configuration.
python3 main.py
```

## Expected Result 
- The fp_staging table is updated.
- The dataset table is updated.
- The dataset is uploaded to s3.

## NOTE

- House keeping of the temporary storage for datasets awaiting labeling has not been implemented.

